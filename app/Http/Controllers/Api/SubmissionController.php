<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSubmissionRequest;
use App\Models\Submission;
use App\Notifications\ContactNotification;
use Notification;

class SubmissionController extends Controller
{
    public function send(StoreSubmissionRequest $request)
    {
        $validated = $request->validated();
        $success = false;
        $validKey = $validated['key'] === config('mail.key');
        $honeypot = ! $validated['honeypot'];
        $submission = Submission::make([
            ...$validated,
            'ip' => $request->ip(),
        ]);

        if ($honeypot && $validKey) {
            $submission->save();
            Notification::route('mail', $submission->to)
                ->notify(new ContactNotification($submission))
            ;
            $success = true;
        }

        return response()->json([
            'message' => $success ? 'Submssion received.' : 'Submission failed.',
            'success' => [
                'honeypot' => $honeypot,
                'key' => $validKey,
            ],
            'submission' => [
                'name' => $submission->name,
                'email' => $submission->email,
                'message' => $submission->message,
            ],
        ]);
    }
}
